package b137.abano.s02d2;
import java.util.ArrayList; // import the ArrayList class

public class ArrayLists {
    public static void main (String[] args) {
        System.out.println("ArrayLists\n");

        //Syntax
        //ArrayLists<data_type><identifier> = new ArrayLists<data_type>();
        ArrayList<String> TheBeatles = new ArrayList<>();

        //inserting new item(s) in an ArrayList & retrieving existing item(s) in an array list.
        TheBeatles.add("John");
        TheBeatles.add("Paul");
        TheBeatles.add("George");
        TheBeatles.add("Ringo");

        System.out.println(TheBeatles.get(0));
        System.out.println(TheBeatles);

        //updating item(s) of an array list
        TheBeatles.set(0, "John Lennon");

        System.out.println(TheBeatles.get(0));
        System.out.println(TheBeatles);

        //removing an existing item in an array list
        TheBeatles.add("Yoko Uno");
        System.out.println(TheBeatles);
        TheBeatles.remove(4);
        System.out.println(TheBeatles);

        //removes all items in an array list
        TheBeatles.clear();
        System.out.println(TheBeatles);
    }
}
